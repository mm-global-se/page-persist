var modifiers = {
  pagePersist: function (data) {
    // concat data
    if (!~(this.storedCampaignInfo || '').indexOf(data.campaignInfo)) {
        this.storedCampaignInfo = this.storedCampaignInfo ?
                                    (this.storedCampaignInfo + '&') : '';
        this.storedCampaignInfo += data.campaignInfo;
    }

    // update campaign info
    data.campaignInfo = this.storedCampaignInfo;
  }
};