# Overview

Modifier method that allows you to concatenate __one-page experiences__ data before it is sent to third-party

__pagePersist(data, integration)__

Parameters:

Name        | Type   | Description
:----------:| :----: | :--------------: |
data        | Object | Campaign Data    |
integration | String | Integration Name |

# Download

[page-persist.js](https://bitbucket.org/mm-global-se/page-persist/src/master/src/page-persist.js)

#Deployment Details

* Take modifier method code from [here](https://bitbucket.org/mm-global-se/page-persist/src/master/src/page-persist.js)

* Add it to `defaults.modifiers` object inside integration registration method

_Example_:

```javascript

mmcore.IntegrationFactory.register('Google Analytics', {
  defaults: {
    modifieres: {
      pagePersist: function (data) {
        // concat data
        if (!~(this.storedCampaignInfo || '').indexOf(data.campaignInfo)) {
            this.storedCampaignInfo = this.storedCampaignInfo ?
                                        (this.storedCampaignInfo + '&') : '';
            this.storedCampaignInfo += data.campaignInfo;
        }

        // update campaign info
        data.campaignInfo = this.storedCampaignInfo;
      }
    },

    isStopOnDocEnd: true
  },

  // ...
});

```